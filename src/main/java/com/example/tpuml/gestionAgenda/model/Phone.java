package com.example.tpuml.gestionAgenda.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Phone extends ContactDetail {
    public Phone(String value, String type) {
        super(value, type);
    }

    public boolean validate(String phoneInput) {
        Pattern pattern = Pattern.compile("\\d{10,}");
        Matcher matcher = pattern.matcher(phoneInput);
        boolean matchFound = matcher.find();
        return matchFound;
    }
}
