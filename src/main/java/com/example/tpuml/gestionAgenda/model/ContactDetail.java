package com.example.tpuml.gestionAgenda.model;

public abstract class ContactDetail {
    public String value;
    public Contact owner;
    public String type;

    public ContactDetail(String value, String type) {
        this.value = value;
        this.type = type;
    }

    public boolean validate(String input) {

        return false;
    }
}
