package com.example.tpuml.gestionAgenda.model;

import java.util.ArrayList;
import java.util.List;

public class Contact {
    public String name;
    private List<ContactDetail> adresses;

    public Contact(String name) {
        this.name = name;
        this.adresses = new ArrayList<ContactDetail>();
    }

    public void addContactDetail(String value, String type){
        //TODO Finaliser la méthode pour ajouter les contactdetails

//            switch(type) {
//                case "PHONE":
//                    Phone phone = new Phone(value, type);
//                    if(phone.validate(value)){
//                        this.adresses.add(phone);
//                    }
//                case ""
//        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ContactDetail> getAdresses() {
        return adresses;
    }

    public void setAdresses(List<ContactDetail> adresses) {
        this.adresses = adresses;
    }


}
