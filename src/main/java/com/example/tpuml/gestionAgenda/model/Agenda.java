package com.example.tpuml.gestionAgenda.model;

import java.util.ArrayList;
import java.util.List;

public class Agenda {

    private List<Contact> contacts;

    private String name;

    public Agenda(String name) {
        this.name = name;
        this.contacts = new ArrayList<Contact>();
    }

    public Contact createContact(String name) {
        return new Contact(name);
    }

    public void ajouterContact(Contact contact) {
        System.out.println("ajout du contact " + contact.getName() + " dans l'agenda");
        this.contacts.add(contact);
    }

    public void modifierNomContact(Contact contact, String newName) {
        System.out.println("Modification du nom du contact");
        contact.setName(newName);
    }

    public void supprimerContact(Contact contact) {
    //TODO NYI
    }

    public void afficherContact() {
        System.out.println("Affichage des contacts de l'agenda");
        for (Contact contact : contacts
        ) {
            System.out.println(contact.getName());
        }
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
