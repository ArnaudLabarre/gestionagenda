package com.example.tpuml.gestionAgenda.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Adress extends ContactDetail {

    public Adress(String value, String type) {
        super(value, type);
    }

    public boolean validate(String addressInput) {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9\\s,'-]*$");
        Matcher matcher = pattern.matcher(addressInput);
        boolean matchFound = matcher.find();
        return matchFound;
    }
}
