package com.example.tpuml.gestionAgenda.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Website extends ContactDetail{

    public Website(String value, String type) {
        super(value, type);
    }

    public boolean validate(String websiteInput) {
        Pattern pattern = Pattern.compile("https?://(www.)?[-a-zA-Z0-9@:%.+~#=]{1,256}.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%+.~#?&//=]*)");
        Matcher matcher = pattern.matcher(websiteInput);
        boolean matchFound = matcher.find();
        return matchFound;

    }
}
