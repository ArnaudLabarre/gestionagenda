package com.example.tpuml.gestionAgenda.model;


import java.util.ArrayList;
import java.util.List;

public class User {
    public String login;
    private String password;
    private String jwt_token;
    private List<Agenda> agendas;

    public void creerAgenda(String name){
        Agenda agenda = new Agenda(name);
        this.agendas.add(agenda);
    }

    public void updateAgenda(){
        //TODO NYI
    }

    public void deleteAgenda(){
        //TODO NYI
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
        this.jwt_token = jwt_token;
        this.agendas = new ArrayList<Agenda>();
        this.agendas.add(new Agenda("DefaultAgenda"));
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJwt_token() {
        return jwt_token;
    }

    public void setJwt_token(String jwt_token) {
        this.jwt_token = jwt_token;
    }

    public List<Agenda> getAgendas() {
        return agendas;
    }

    public void setAgendas(List<Agenda> agendas) {
        this.agendas = agendas;
    }
}
