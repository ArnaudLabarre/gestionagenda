package com.example.tpuml.gestionAgenda.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email extends ContactDetail {

    public Email(String value, String type) {
        super(value, type);
    }

    public boolean validate(String emailInput) {
        Pattern pattern = Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
        Matcher matcher = pattern.matcher(emailInput);
        boolean matchFound = matcher.find();
        return matchFound;
    }
}
