package com.example.tpuml.gestionAgenda;

import com.example.tpuml.gestionAgenda.model.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TpUmlApplication {

    public static void main(String[] args) {

        Adress adress = new Adress("13 boulevard de la Tour","ADDRESS");
        Phone phone = new Phone ("0299996585", "PHONE");
        Email email = new Email("testEmail@test.com", "EMAIL");
        Website website = new Website("https://testurl.42testing.com", "WEBSITE");
        User user = new User("SuperUser", "pswd");

        user.getAgendas().get(0).getContacts();
        Agenda agenda = user.getAgendas().get(0);

        agenda.ajouterContact(agenda.createContact("André"));
        agenda.ajouterContact(agenda.createContact("Jérome"));
        agenda.afficherContact();

    }


}
