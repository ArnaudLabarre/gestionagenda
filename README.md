## Environnement

La version de java utilisée est la 18.0.2

## Lancement du projet

- Se placer au niveau du dossier racine du projet dans le terminal
- Exécuter la commande suivante :
    java -jar .\target\tpUML-0.0.1-SNAPSHOT.jar

## Diagramme de classe final

![alt](./src/main/resources/diagrammeClasseFinal.png)